import { Component, OnInit } from '@angular/core';
import { Student } from '../../shared/interfaces/student';
import { StudentService } from '../../shared/services/student/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {

  constructor(private studentService: StudentService) { }

  students: Student[];

  ngOnInit() {
    this.studentService.getAllStudents()
      .subscribe(students => {
        console.log(`${JSON.stringify(students)}`);
        this.students = students
      });
  }

  newStudent: Student = {
    firstName: null,
    lastName: null,
    grades: [],
    id: null
  };

  public addStudent() {
    const newStudent = { ...this.newStudent };
    this.studentService.createStudent(newStudent)
        .subscribe(student => {
          this.students.push(student);
          this.newStudent.firstName = '';
          this.newStudent.lastName = '';
        });
  }
  public removeStudent(student: Student): void {
    this.studentService.removeStudent(student)
        .subscribe(() => {
          this.studentService.getAllStudents()
            .subscribe(students => this.students = students);
        });
              
  }

}
