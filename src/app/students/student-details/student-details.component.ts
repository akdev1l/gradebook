import { Component, OnInit } from '@angular/core';
import { Student } from '../../shared/interfaces/student';
import { StudentService } from '../../shared/services/student/student.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss']
})
export class StudentDetailsComponent implements OnInit {

  constructor(private studentsService: StudentService,
              private activatedRoute: ActivatedRoute) { }
  student: Student;

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const studentId = parseInt(params.get('id'));
        return this.studentsService.getStudent(studentId);
      })
    ).subscribe(student => {
      this.student = student;
      console.log(this.student);
    });
  }

  private editGrade(id: number) {

  }

}
