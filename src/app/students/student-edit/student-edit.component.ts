import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { StudentService } from '../../shared/services/student/student.service';
import { switchMap } from 'rxjs/operators';
import { Student } from '../../shared/interfaces/student';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.scss']
})
export class StudentEditComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private studentsService: StudentService,
              private router: Router) { }

  student: Student;
  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const studentId = parseInt(params.get('id'));
        return this.studentsService.getStudent(studentId);
      })
    ).subscribe(student => this.student = student);
  }

  updateStudent() {
    this.studentsService.updateStudent(this.student).subscribe(() => {
      this.router.navigateByUrl(`student/${this.student.id}`);
    });
  }

}
