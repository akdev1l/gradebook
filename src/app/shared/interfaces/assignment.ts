import { Student } from "./student";
import { Grade } from "./grade";

export enum AssignmentType {
    Assignment = "Assignment",
    Test = "Test"
}

export interface Assignment {
    id: number;
    type: AssignmentType;
    description: string;
    weight: number;
    grades: Grade[];
    average?: Grade;
}
