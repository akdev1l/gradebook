import { Assignment } from "./assignment";
import { Student } from "./student";

export interface Program {
    students: Student[];
    assignments: Assignment[];
    title: string;
    code: string;
}
