import { Assignment } from "./assignment";
import { Student } from "./student";

export interface Grade {

    id: number;
    symbol: string;
    score: number;
    assignment: Assignment;
    author: Student;
}
