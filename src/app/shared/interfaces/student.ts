import { Grade } from "./grade";
import { Assignment } from "./assignment";

export interface Student {

    id: number;
    firstName: string;
    lastName: string;
    grades?: Grade[];
}
