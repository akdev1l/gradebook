import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Assignment, AssignmentType } from '../../interfaces/assignment';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../api';
import { Grade } from '../../interfaces/grade';

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {


  mockAssignments: Assignment[] = [
  ];

  constructor(
    private http: HttpClient
  ) { }

  public getAssignments(): Observable<Assignment[]> {
    return this.http.get<Assignment[]>(ApiUrl.getAllAssignments);
  }
  public createAssignment(assignment: Assignment): Observable<Assignment> {
    assignment.id = this.mockAssignments.length;
    this.mockAssignments = this.mockAssignments.concat(assignment);
    return Observable.create(observer => observer.next(assignment));
  }
  public getAssignmentTypes(): Observable<string[]> {
    return Observable.create(observer => observer.next(Object.keys(AssignmentType)));
  }
  public removeAssignment(assignment: Assignment): Observable<void> {
    return this.http.delete<void>(`${ApiUrl.removeAssignment}/${assignment.id}`);
  }
  private getById(id: number) {
    return this.mockAssignments[id % this.mockAssignments.length];
  }
  public getAssignment(id: number): Observable<Assignment> {
    return this.http.get<Assignment>(`${ApiUrl.getAssignment}/${id}`);
  }
  public addGrade(assignmentId: number, grade: Grade): Observable<Grade[]> {
    if(assignmentId != assignmentId) {
      return Observable.create((observer) => observer.error(`${assignmentId} is not a number`));
    }

    return this.http.post<Grade[]>(ApiUrl.AddGrade.replace(":assignmentId", `${assignmentId}`), grade);
  }
}
