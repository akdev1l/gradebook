export const BaseApiUrl = 'https://vast-atoll-28929.herokuapp.com/api';
export const ApiUrl = {
  getAllStudents: `${BaseApiUrl}/students`,
  getStudent: `${BaseApiUrl}/student`,
  removeStudent: `${BaseApiUrl}/student`,
  createStudent: `${BaseApiUrl}/student/create`,
  updateStudent: `${BaseApiUrl}/student`,

  getAllAssignments: `${BaseApiUrl}/assignments`,
  getAssignment: `${BaseApiUrl}/assignment`,
  removeAssignment: `${BaseApiUrl}/assignment`,
  AddGrade: `${BaseApiUrl}/assignment/:assignmentId/grades/add`,
  getGrade: `${BaseApiUrl}/grade`,
  updateGrade: `${BaseApiUrl}/grade`,
};
