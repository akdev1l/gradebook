import { Injectable } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Student } from '../../interfaces/student';
import { Grade } from '../../interfaces/grade';
import { Assignment, AssignmentType } from '../../interfaces/assignment';
import { AssignmentService } from '../assignment/assignment.service';
import { ApiUrl } from '../api';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(
    private assignmentService: AssignmentService,
    private http: HttpClient
    ) {
  }

  mockData: Student[] = [
  ];

  public getAllStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(ApiUrl.getAllStudents);
  }
  public getStudent(id: number): Observable<Student> {
    if(id != id) return;
    
    return this.http.get<Student>(`${ApiUrl.getStudent}/${id}`);
  }
  private getById(id: number): Student {
    return this.mockData[id % this.mockData.length];
  }
  public updateStudent(student: Student): Observable<void> {
    return this.http.patch<void>(ApiUrl.updateStudent, student);
  }
  public createStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(ApiUrl.createStudent, student);
  }

  public removeStudent(student: Student): Observable<void>{
    this.mockData = this.mockData.filter(stud => stud.id != student.id);
    return this.http.delete<void>(`${ApiUrl.removeStudent}/${student.id}`);
  }
}
