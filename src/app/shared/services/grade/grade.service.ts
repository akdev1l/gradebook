import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../api';
import { Grade } from '../../interfaces/grade';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GradeService {

  constructor(
    private http: HttpClient
  ) { }

  public getGrade(id: number): Observable<Grade> {
    return this.http.get<Grade>(`${ApiUrl.getGrade}/${id}`);
  }
  public updateGrade(grade: Grade): Observable<void> {
    return this.http.patch<void>(ApiUrl.updateGrade, grade);
  }
}
