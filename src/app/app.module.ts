import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import "reflect-metadata";

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { StudentListComponent } from './students/student-list/student-list.component';
import { StudentDetailsComponent } from './students/student-details/student-details.component';
import { AssignmentListComponent } from './assignments/assignment-list/assignment-list.component';
import { AssignmentEditComponent } from './assignments/assignment-edit/assignment-edit.component';
import { StudentEditComponent } from './students/student-edit/student-edit.component';
import { AssignmentDetailsComponent } from './assignments/assignment-details/assignment-details.component';
import { HttpClientModule } from '@angular/common/http';
import { GradeEditComponent } from './grade/grade-edit/grade-edit.component';

const routes: Routes = [
  {
    path: '',
    component: StudentListComponent,
  },
  {
    path: 'students',
    component: StudentListComponent
  },
  {
    path: 'student/:id',
    component: StudentDetailsComponent
    
  },
  {
    path: 'student/:id/edit',
    component: StudentEditComponent
  },
  {
    path: 'assignments',
    component: AssignmentListComponent
  },
  {
    path: 'assignment/:id',
    component: AssignmentDetailsComponent
  },
  {
    path: 'grade/:id/edit',
    component: GradeEditComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StudentListComponent,
    StudentDetailsComponent,
    AssignmentListComponent,
    AssignmentEditComponent,
    StudentEditComponent,
    AssignmentDetailsComponent,
    GradeEditComponent,
  ],
  imports: [
    RouterModule.forRoot(routes, { enableTracing: true }),
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
