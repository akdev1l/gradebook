import { Component, OnInit } from '@angular/core';
import { AssignmentService } from '../../shared/services/assignment/assignment.service';
import { Assignment } from '../../shared/interfaces/assignment';

@Component({
  selector: 'app-assignment-list',
  templateUrl: './assignment-list.component.html',
  styleUrls: ['./assignment-list.component.scss']
})
export class AssignmentListComponent implements OnInit {

  constructor(private assignmentService: AssignmentService) { }

  assignments: Assignment[];
  newAssignment: Assignment = {
    description: null,
    id: -1,
    type: undefined,
    weight: null,
    grades: []
  };
  assignmentTypes: string[];

  ngOnInit() {
    this.assignmentService.getAssignments()
        .subscribe(assignments => this.assignments = assignments);

    this.assignmentService.getAssignmentTypes()
        .subscribe(types => this.assignmentTypes = types);
  }

  public addAssignment() {
    const newAssignment = { ...this.newAssignment };
    this.assignmentService.createAssignment(newAssignment)
        .subscribe(assignment => {
          this.assignments.push(assignment);
          this.newAssignment = {
            description: '',
            id: -1,
            type: undefined,
            weight: null,
            grades: []
          };
        });
  }

  private removeAssignment(assignment: Assignment) {
    this.assignmentService.removeAssignment(assignment)
        .subscribe(() => {
          this.assignmentService.getAssignments()
            .subscribe(assignments => this.assignments = assignments);
        });
  }

}
