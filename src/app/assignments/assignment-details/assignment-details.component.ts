import { Component, OnInit } from '@angular/core';
import { Assignment } from '../../shared/interfaces/assignment';
import { AssignmentService } from '../../shared/services/assignment/assignment.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Grade } from '../../shared/interfaces/grade';
import { Student } from '../../shared/interfaces/student';
import { StudentService } from '../../shared/services/student/student.service';

@Component({
  selector: 'app-assignment-details',
  templateUrl: './assignment-details.component.html',
  styleUrls: ['./assignment-details.component.scss']
})
export class AssignmentDetailsComponent implements OnInit {

  constructor(
    private studentService: StudentService,
    private assignmentService: AssignmentService,
    private route: ActivatedRoute
  ) { }

  assignment: Assignment;
  newGrade: Grade = {
    assignment: this.assignment,
    author: null,
    id: null,
    score: null,
    symbol: null,
  };
  students: Student[] = [];

  ngOnInit() {
    this.route.paramMap
        .pipe(
          switchMap((params: ParamMap) => {
            const assignmentId = parseInt(params.get('id'));
            return this.assignmentService.getAssignment(assignmentId);
          })
        )
        .subscribe(assignment => this.assignment = assignment);
    this.studentService.getAllStudents().subscribe(students => this.students = students);
  }
  
  public addGrade() {
    this.assignmentService.addGrade(this.assignment.id, this.newGrade)
      .subscribe(newGrades => {
        this.assignment.grades = newGrades;
        this.newGrade = {
          assignment: this.assignment,
          author: null,
          id: null,
          score: null,
          symbol: null,
        };
        this.assignmentService.getAssignment(this.assignment.id).subscribe(assignment => this.assignment = assignment);
      });
  }

}
