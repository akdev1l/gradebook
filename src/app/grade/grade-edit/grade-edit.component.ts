import { Component, OnInit } from '@angular/core';
import { Grade } from '../../shared/interfaces/grade';
import { HttpClient } from '@angular/common/http';
import { GradeService } from '../../shared/services/grade/grade.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-grade-edit',
  templateUrl: './grade-edit.component.html',
  styleUrls: ['./grade-edit.component.scss']
})
export class GradeEditComponent implements OnInit {

  constructor(
    private gradeServive: GradeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  grade: Grade;

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const gradeId = parseInt(params.get('id'));
      this.gradeServive.getGrade(gradeId)
          .subscribe(grade => {
            this.grade = grade;
      });
      
    })
  }

  private updateGrade() {
    console.log(JSON.stringify(this.grade));
    this.gradeServive.updateGrade(this.grade)
        .subscribe(() => {
          this.gradeServive.getGrade(this.grade.id)
              .subscribe(grade => this.router.navigateByUrl(`/student/${grade.author.id}`));
        });
  }

}
