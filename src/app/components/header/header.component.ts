import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})


export class HeaderComponent implements OnInit {


  readonly studentActions = [
    { name: 'All Students', url: '/students' },
  ];
  readonly assignmentActions = [
    { name: 'All Assignments', url: '/assignments' },
    // { name: 'Search Student', url: '/' },
    // { name: 'Delete Student', url: '/' },
  ];
  constructor() { }

  ngOnInit() {
  }

}
